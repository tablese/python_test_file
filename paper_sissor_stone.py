from random import randint
import os

def cls():
    os.system('cls' if os.name == 'nt' else 'clear')

def check_input(input_value, ar_input):
    return input_value in ar_input

def check_result(action):
    chosen = randint(0,2)
    print('Player[%s] VS Computer[%s]' % (player[action], computer[chosen]))

    if computer[chosen] == player[action]:
        return 'd'
    elif (chosen == 0 and action == 's') or (chosen == 1 and action == 'r') or (chosen == 2 and action == 'p'):
        return 'w'
    elif (chosen == 0 and action == 'r') or (chosen == 1 and action == 'p') or (chosen == 2 and action == 's'):
        return 'l'

def winner(matches):
    win, lose = 0, 0
    print_title = '%s\t\t%d\t\t%d\t\t%s'
    print('This is final result.')
    print('[ %s ][ %s ]\t [ %s ] \t[ %s ]' % ('Match', 'Computer', player_name, 'Winner'))
    for match in matches:
        win = win + match['result']['player']
        lose = lose + match['result']['computer']
        print(print_title % (match['m_name'], match['result']['computer'], match['result']['player'], match['result']['winner']))
    
    if win>lose:
        return 'w'
    elif lose>win:
        return 'l'
    else:
        return 'd'


def play_game(count):
    cls()
    print('OK. Let\'s start')
    for i in range(count):
        user_input = str(raw_input('For Match %d\nYour Turn: Paper (p), or Scissors (s), or Rock (r)?: ' % (i+1)))
        while check_input(str.lower(user_input), game_input_action) == False:
            print('Sorry. Please only type in p/s/r to choose.\n')
            user_input = str(raw_input('For Match %d\nYour Turn: Paper (p), or Scissors (s), or Rock (r)?: ' % (i+1)))
            cls()
        cls()
        result = check_result(str.lower(user_input))
        match.append(
            {
                'm_name': ('Match %d' % (i+1)), 
                'result':{
                    'computer': 1 if result == 'l' else 0, 
                    'player': 1 if result == 'w' else 0, 
                    'winner': match_status[result]                    
                }
            })
        match_title = 'Result' if result == 'd' else 'Winner'
        print('%s of %s is %s' % (match_title, match[i]['m_name'], match[i]['result']['winner']))

def get_player_name():
    p_name = raw_input('Welcome from Paper Scissors Rock game.\nYour name, please? : ')
    while len(p_name) == 0:
        p_name = raw_input('Sorry..I didn\'t caught your name.\nYour name again, please? : ')
    return p_name

def game_count():
    game = raw_input('How many match that you\'d like to play? : ')
    while  game.isdigit() == False:
        print(game.isdigit())
        print('Sorry. You did\'t type a digit number [0~9]. Please try again.\n')
        game = raw_input('How many match that you\'d like to play? : ')
    play_game(int(game))
        
    

def start_game():
    cls()
    final_result = winner(match)
    if final_result == 'w':
        print('Finally. Winner is You. Congratulation... %s' % player_name)
    elif final_result == 'l':
        print('Finally. Winner is Me. Sorry ... %s' % player_name)
    else:
        print('Ohh.. Final Result is Draw! ... Great!')

if __name__ == "__main__":
    game_input_action = ['p', 's', 'r']
    more_game_input_action = ['y', 'n']

    computer = ['Paper', 'Scissors', 'Rock']
    player = {'p': 'Paper', 's': 'Scissors', 'r': 'Rock'}
    match = []
    match_status = {'d' : 'Draw', 'w': 'Player', 'l': 'Computer'}
    cls()
    player_name = get_player_name();
    match_status['w'] = player_name
    game_count();

    start_game();
    more_game = raw_input('Do you like to play more Yes (y) or No (n)? : ')
    while  check_input(str.lower(more_game), more_game_input_action) == False:
        print('Sorry. Please only type in y or n to choose.\n')
    while str.lower(more_game) == 'y':
        match=[]
        cls()
        game_count();
        start_game();
        more_game = raw_input('Do you like to play more Yes (y) or No (n)? : ')